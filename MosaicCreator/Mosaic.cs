using System;
using System.IO;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;

namespace MosaicCreator
{
	public class Mosaic
	{
		private int width;
		private int height;
		private Bitmap mainImage;
		private Bitmap resultImage;
		private Color[][] buffer;
		private static int indexBuffer =0;
		private int sizeBuffer =5;
		private static Hashtable colors;	// Key is color & Value is image From File
		private static String tilesDirectory = AppDomain.CurrentDomain.BaseDirectory + "data";
		public int tpw {set; get;}
		public int tph{set; get;}

		/* Properties */
		public static String TilesDirectory {
			get { return Mosaic.tilesDirectory; }
			set {
				if (value != "") {
					Mosaic.tilesDirectory = value;
				}
			}
		}

		public Bitmap Image {
			set {
				if (value != null)
					this.mainImage = value;
			}
		}

		public int Width {
			get { return width; }
			set {
				if (value > 0)
					this.width = value;
			}
		}

		public int Height {
			get { return height; }
			set {
				if (value > 0)
					this.height = value;
			}
		}

		// Constractor get mainImage, width and height of the result image
		public Mosaic (Image mainImage, int width, int height)
		{
			this.width 		= width;
			this.height 	= height;
			this.mainImage 	= new Bitmap(mainImage);
			buffer			= new Color [sizeBuffer][] ;
		}

		public static void initialize (int squareSize)
		{
			initialize(squareSize, squareSize);
		}

		public static void initialize (int width, int height)
		{
			colors 			= new Hashtable();

			// Load image for mosaic from tilesDirectory
			String[] imgs = Directory.GetFiles(tilesDirectory);

			foreach (String imgDirectory in imgs) {
				// Resize them using ResizeBitmap & width = height = squareSize
				Bitmap img;
				try {
					img = new Bitmap(Image.FromFile(imgDirectory));
				} catch(Exception) {
					MessageBox.Show("Error: Could not load image!\n" + imgDirectory);
					continue;
				}
				img = ResizeBitmap(img, width, height);
				int red = 0; int green = 0; int blue = 0;
				for (int x=0; x<width; x++) {
					for (int y=0; y<height; y++) {
						// Choose the value of red, blue or green
						Color pixelColor = img.GetPixel(x, y);
						red += pixelColor.R; green += pixelColor.G; blue += pixelColor.B;
					}
				}
				// Add image to hashtable
				Color temp = Color.FromArgb(((int)red/(width*height)), 
				                          ((int)green/(width*height)), 
				                            ((int)blue/(width*height))) ;

				if (colors[temp]==null)
					colors.Add(temp,img);
			}
		}

		// Resize bitmap image
		private static Bitmap ResizeBitmap (Bitmap sourceBMP, int width, int height)
		{
			Bitmap result = new Bitmap (width, height);
			using (Graphics g = Graphics.FromImage(result))
				g.DrawImage (sourceBMP, 0, 0, width, height);
			return result;
		}

		private bool isUsed (Color key, int i, int j)
		{
			for (int ii=0; ii<sizeBuffer; ii++)
				for (int jj=j; jj>j-sizeBuffer&jj>=0; jj--)
					if (buffer[ii][jj] == key)
						return true ;
			return false ;
		}

		// Get image from the hashtable independent on pixels color
		private Bitmap getImage (Color pixels,int i ,int j)
		{
			// Find smallest diffrent with main image
			Color keyColor = Color.FromArgb(255, 255, 255);
			int minDiff = int.MaxValue;
			foreach (Color c in colors.Keys) {
				//To exclude last used images
				if (!isUsed(c,i,j)){
					int diff = Math.Abs(pixels.R - c.R) +
							Math.Abs(pixels.G - c.G) +
							Math.Abs(pixels.B - c.B);
					if (minDiff > diff) {
						minDiff = diff;
						keyColor = c;
					}
				}
			}

			//Full buffer with the last used color
			if (indexBuffer >= sizeBuffer)
				indexBuffer = 0 ;
			buffer[indexBuffer++][j] = keyColor ;

			// Get the bitmap
			return ((Bitmap) colors[keyColor]);
		}

		// Get mosaic image for main image
		public Bitmap getMosaic (ProgressBar progressBar = null)
		{
			int red = 0;
			int green = 0;
			int blue = 0;

			resultImage = new Bitmap (width, height);
			Graphics gr = Graphics.FromImage (resultImage);
			// Get number of tiles in each row and each column
			int tilesNumberInRow = (int)Math.Round ((double)width / tpw);
			int tilesNumberInColumn = (int)Math.Round ((double)height / tph);

			for (int i=0; i<sizeBuffer; i++)
				buffer[i] = new Color[tilesNumberInRow];

			if (progressBar != null) {
				progressBar.Minimum = progressBar.Value = 0;
				progressBar.Maximum = tilesNumberInRow * tilesNumberInColumn;
			}

			// Get new width and height independent on number of tiles in each row and column
			width = tilesNumberInRow * tpw;
			height = tilesNumberInColumn * tph;

			// Resize main image
			mainImage = ResizeBitmap (mainImage, width, height);
			resultImage = ResizeBitmap(resultImage, width, height);
			gr = Graphics.FromImage (resultImage);

			// For each tiles in row
			for (int i=0; i<tilesNumberInColumn; i++) {
				// For each tiles in column
				for (int j=0; j<tilesNumberInRow; j++) {
					// For each pixel in main image
					for (int x = j*tpw; x < (j+1)*tpw; x++) {
						for (int y = i*tph; y < (i+1)*tph; y++) {
							// calculate red, green and blue
							red += mainImage.GetPixel (x, y).R;
							green += mainImage.GetPixel (x, y).G;
							blue += mainImage.GetPixel (x, y).B;
						}
					}

					// Calculate the avarage
					red = (int)(red / (tpw * tph));
					green = (int)(green / (tpw * tph));
					blue = (int)(blue / (tpw * tph));

					// Add image to the result image on it's position
					Bitmap tile = getImage(Color.FromArgb(red, green, blue),i,j);
					gr.DrawImage (tile, j * tpw, i * tph);

					// Set new for red, green & blue
					red = green = blue = 0;

					if (progressBar != null)
						progressBar.Value += 1;
				}
			}
			return resultImage;
		}
	}
}

