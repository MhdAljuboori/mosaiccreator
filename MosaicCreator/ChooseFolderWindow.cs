using System;
using System.Windows.Forms;

namespace MosaicCreator
{
	public class ChooseFolderWindow : Form
	{
		FolderBrowserDialog chooseFolderDialog;
		Label				chooseFolderDescription;
		TextBox				folderDirectoryTextBox;
		Button				chooseFolderButton;

		String folderDirectory = Mosaic.TilesDirectory;
		int width = 15;
		int height = 15;

		public ChooseFolderWindow (int width, int height)
		{
			Text = "Folder Settings";
			Size = new System.Drawing.Size(300, 140);
			Closed += new EventHandler(formClosed);
			MaximizeBox = false;
			MinimizeBox = false;
			FormBorderStyle = FormBorderStyle.FixedDialog;
			StartPosition = FormStartPosition.CenterScreen;

			this.width = width;
			this.height = height;

			// choose folder Dialog
			chooseFolderDialog = new FolderBrowserDialog();

			// Label for choose folder
			chooseFolderDescription = new Label();
			chooseFolderDescription.Text = "Choose folder of tiles images: ";
			chooseFolderDescription.Location = new System.Drawing.Point(10, 10);
			chooseFolderDescription.Size = new System.Drawing.Size(200, 20);
			chooseFolderDescription.Parent = this;

			// Text Box for folder directory
			folderDirectoryTextBox = new TextBox();
			folderDirectoryTextBox.Text = folderDirectory;
			folderDirectoryTextBox.Location = new System.Drawing.Point(10, 35);
			folderDirectoryTextBox.Size = new System.Drawing.Size(270, 30);
			folderDirectoryTextBox.Parent = this;

			// Button for choose folder
			chooseFolderButton = new Button();
			chooseFolderButton.Text = "Browse Folder";
			chooseFolderButton.Location = new System.Drawing.Point(10, 70);
			chooseFolderButton.Size = new System.Drawing.Size(270, 30);
			chooseFolderButton.Click += new EventHandler(chooseFolderButtonClicked);
			chooseFolderButton.Parent = this;
		}

		void chooseFolderButtonClicked (object sender, EventArgs e)
		{
			chooseFolderDialog.ShowDialog();
			folderDirectory = chooseFolderDialog.SelectedPath;
			folderDirectoryTextBox.Text = folderDirectory;
		}

		void formClosed (object sender, EventArgs e)
		{
			Mosaic.TilesDirectory = folderDirectory;
			Mosaic.initialize(width, height);
		}
	}
}

