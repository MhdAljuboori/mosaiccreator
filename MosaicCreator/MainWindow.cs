using System;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.ComponentModel;

namespace MosaicCreator
{
	public class MainWindow : Form
	{
		OpenFileDialog	openImageDialog;
		SaveFileDialog	saveImageDialog;
		TextBox 		imageURI;
		Button			browse;
		Button			createMosaic;
		Button			saveImage;
		PictureBox 		mosaicImage;
		Label			resolutionLabel;
		Label			descriptionResLabel;
		ComboBox		resolutionValue;
		Label			dimentionsLabel;
		Label			XLabel;
		TextBox			widthTextBox;
		TextBox			heightTextBox;
		StatusBar		mainImageDimention;
		String			imageDimentionString = "Main Image Dimentions";
		ProgressBar		creatingMosaicProgressBar;
		TextBox			tilesPerRow;
		TextBox			tilesPerColumn;
		Button			advanceButton;
		Button			chooseFolderButton;

		private void InitializeComponent ()
		{
			// Open File Dialog
			openImageDialog = new OpenFileDialog ();
			openImageDialog.Filter = "Image Files(*.BMP;*.JPG;*.PNG)|*.BMP;*.bmp;*.JPG;*jpg;*.PNG;*.png|All files (*.*)|*.*";
			openImageDialog.ShowDialog ();
			String imageURIText = openImageDialog.FileName;

			// Save Image Dialog
			saveImageDialog = new SaveFileDialog();
			saveImageDialog.Filter = "Image Files(*.PNG)|*.png";
			saveImageDialog.DefaultExt = ".png";

			// Text Box Image URI
			imageURI 			= new TextBox();
			imageURI.Location 	= new Point(10, 10);
			imageURI.ForeColor 	= Color.Blue;
			imageURI.Width		= 300;
			imageURI.Parent		= this;

			// Button for changing image
			browse 			= new Button();
			browse.Text 	= "Browse";
			browse.Location = new Point(320, 10);
			browse.Click   += new EventHandler(browseImageButtonClicked);
			browse.Parent	= this;

			// Button for creating the mozaic
			createMosaic 			= new Button();
			createMosaic.Text 		= "Create Mozaic";
			createMosaic.Location 	= new Point(400, 10);
			createMosaic.Width		= 100;
			createMosaic.Click	   += new EventHandler(createMosaicButtonClicked);
			createMosaic.Parent		= this;

			// Button for saving image
			saveImage 			= new Button();
			saveImage.Text 		= "Save Image";
			saveImage.Width 	= 80;
			saveImage.Location 	= new Point(505, 10);
			saveImage.Click	   += new EventHandler(saveImageButtonClicked);
			saveImage.Parent 	= this;

			// Label of resolution
			resolutionLabel 		 = new Label();
			resolutionLabel.Text 	 = "Resolution";
			resolutionLabel.Location = new Point(10, 50);
			resolutionLabel.Size 	 = new Size(60, 20);
			resolutionLabel.Parent 	 = this;

			// List Box
			resolutionValue = new ComboBox();
			resolutionValue.Location = new Point(75, 45);
			resolutionValue.Items.Add("Perfect (1)");
			resolutionValue.Items.Add("V. Good (5)");
			resolutionValue.Items.Add("Good (10)");
			resolutionValue.Items.Add("Normal (15)");
			resolutionValue.Items.Add("Bad (25)");
			resolutionValue.Items.Add("V. Bad (30)");
			resolutionValue.SelectedItem = "Normal (15)";
			resolutionValue.Size		 = new Size(100, 100);
			resolutionValue.Visible		 = true;
			resolutionValue.SelectedValueChanged += new EventHandler(resolutionValueChanged);
			resolutionValue.Parent		 = this;

			// Text Box for tiles per row
			tilesPerRow 		 = new TextBox();
			tilesPerRow.Text 	 = "15";
			tilesPerRow.Location = new Point(75, 45);
			tilesPerRow.Width 	 = 45;
			tilesPerRow.Visible  = false;
			//tilesPerRow.TextChanged += new EventHandler(resolutionValueChanged);
			tilesPerRow.Parent 	 = this;

			// Text Box for tiles per column
			tilesPerColumn 		 = new TextBox();
			tilesPerColumn.Text 	 = "15";
			tilesPerColumn.Location = new Point(130, 45);
			tilesPerColumn.Width 	 = 45;
			tilesPerColumn.Visible  = false;
			//tilesPerColumn.TextChanged += new EventHandler(resolutionValueChanged);
			tilesPerColumn.Parent 	 = this;

			// Label of description
			descriptionResLabel 		 = new Label();
			descriptionResLabel.Text 	 = "pixels";
			descriptionResLabel.Location = new Point(180, 50);
			descriptionResLabel.Size 	 = new Size(35, 20);
			descriptionResLabel.Parent 	 = this;

			// Choose Folder Button
			chooseFolderButton = new Button();
			chooseFolderButton.Text = "F";
			chooseFolderButton.Location = new Point(218, 45);
			chooseFolderButton.Width = 30;
			chooseFolderButton.Visible = false;
			chooseFolderButton.Click += new EventHandler(chooseFolderButtonClicked);
			chooseFolderButton.Parent = this;

			// Label of description
			dimentionsLabel 		 = new Label();
			dimentionsLabel.Text 	 = "Dimentions";
			dimentionsLabel.Location = new Point(250, 50);
			dimentionsLabel.Size 	 = new Size(65, 20);
			dimentionsLabel.Parent 	 = this;

			// Text Box width of the output image
			widthTextBox			= new TextBox();
			widthTextBox.Location 	= new Point(325, 45);
			widthTextBox.Text		= "1000";
			widthTextBox.Width		= 70;
			//widthTextBox.TextChanged += new EventHandler(resolutionValueChanged);
			widthTextBox.Parent		= this;

			// Label of description
			XLabel 		 	= new Label();
			XLabel.Text 	= "X";
			XLabel.Location = new Point(405, 50);
			XLabel.Size 	= new Size(10, 20);
			XLabel.Parent 	= this;

			// Text Box height of the output image
			heightTextBox			= new TextBox();
			heightTextBox.Location 	= new Point(420, 45);
			heightTextBox.Text		= "1000";
			heightTextBox.Width		= 70;
			//heightTextBox.TextChanged += new EventHandler(resolutionValueChanged);
			heightTextBox.Parent	= this;

			// Advance Button
			advanceButton 			= new Button();
			advanceButton.Text 		= "Advance";
			advanceButton.Location 	= new Point(510, 45);
			advanceButton.Click	   += new EventHandler(advanceButtonClicked);
			advanceButton.Parent 	= this;

			// Status Bar
			mainImageDimention = new StatusBar();
			mainImageDimention.Text = "No Image Added!!";
			mainImageDimention.Parent = this;

			// Progress Bar
			creatingMosaicProgressBar = new ProgressBar();
			creatingMosaicProgressBar.Width = 200;
			creatingMosaicProgressBar.Location = new Point(380, 0);
			creatingMosaicProgressBar.Parent = mainImageDimention;

			// Picture Box
			mosaicImage 			= new PictureBox();
			mosaicImage.Location 	= new Point(10, 70);
			mosaicImage.Size 		= new Size(570, 550);
			mosaicImage.SizeMode 	= PictureBoxSizeMode.Zoom;
			mosaicImage.Parent 		= this;

			// Load Image
			if (imageURIText != "")
			{
				try {
					// Put image in picture box
					mosaicImage.Image = Image.FromFile(imageURIText);

					//Change the URI of image
					imageURI.Text     = imageURIText;

					// Get width & height of image
					int imageWidth = mosaicImage.Image.Width;
					int imageHeight = mosaicImage.Image.Height;

					// Set width & height in status bar and text boxs
					mainImageDimention.Text = String.Format("{0} {1}×{2}", 
							imageDimentionString, imageWidth, imageHeight);
					widthTextBox.Text = imageWidth.ToString();
					heightTextBox.Text = imageHeight.ToString();
				} catch (System.IO.IOException) {
					MessageBox.Show("Error: Could not load image!\n" + imageURIText);
				}
			}
		}

		public MainWindow ()
		{
			Text = "Mosaic Creator";
			Size = new Size(600, 620);
			MaximizeBox = false;
			MinimizeBox = false;
			FormBorderStyle = FormBorderStyle.FixedDialog;
			StartPosition = FormStartPosition.CenterScreen;
			InitializeComponent();

			Mosaic.initialize(15);
		}

		private int getSquareSize ()//(bool isComboBox)
		{
			// retuern square size independent on resolution mode or resolution number
			int squareSize = 15;
			int index = int.Parse(resolutionValue.SelectedIndex.ToString());
			switch (index) {
			case 0:
				squareSize = 1;
				break;
			case 1:
				squareSize = 5;
				break;
			case 2:
				squareSize = 10;
				break;
			case 3:
				squareSize = 15;
				break;
			case 4:
				squareSize = 25;
				break;
			case 5:
				squareSize = 30;
				break;
			default:
				squareSize = 15;
				break;
			}
			return squareSize;
		}

		private void getDimentions (out int width, out int height)
		{
			int tilesInRow = 0;
			if (!int.TryParse (tilesPerRow.Text.ToString (), out tilesInRow))
				MessageBox.Show ("Enter number please", "Error");
			int tilesInColumn = 0;
			if (!int.TryParse (tilesPerColumn.Text.ToString (), out tilesInColumn))
				MessageBox.Show ("Enter number please", "Error");

			int newHeight = 0;
			if (!int.TryParse (heightTextBox.Text.ToString (), out newHeight))
				MessageBox.Show ("Enter number please", "Error");
			int newWidth = 0;
			if (!int.TryParse (widthTextBox.Text.ToString (), out newWidth))
				MessageBox.Show ("Enter number please", "Error");

			width = (int) (newWidth/(double)tilesInRow);
			height = (int) (newWidth/(double)tilesInColumn);
		}

		void browseImageButtonClicked (object sender, EventArgs e)
		{
			// Open File Dialog
			openImageDialog = new OpenFileDialog ();
			openImageDialog.ShowDialog ();
			String imageURIText = openImageDialog.FileName;

			// Load Image
			if (imageURIText != "")
			{
				try {
					// Put image in picture box
					mosaicImage.Image = Image.FromFile(imageURIText);

					//Change the URI of image
					imageURI.Text     = imageURIText;

					// Get width & height of image
					int imageWidth = mosaicImage.Image.Width;
					int imageHeight = mosaicImage.Image.Height;

					// Set width & height in status bar and text boxs
					mainImageDimention.Text = String.Format("{0} {1}×{2}", 
							imageDimentionString, imageWidth, imageHeight);
					widthTextBox.Text = imageWidth.ToString();
					heightTextBox.Text = imageHeight.ToString();
				} catch (Exception) {
					MessageBox.Show("Error: Could not load image!\n" + imageURIText);
				}
			}
		}

		void saveImageButtonClicked (object sender, EventArgs e)
		{
			if (mosaicImage.Image != null) {
				// Save Image Dialog
				Image image = mosaicImage.Image;
				saveImageDialog.ShowDialog ();
				String imageURIText = saveImageDialog.FileName;

				// Save Image
				if (imageURIText != "") {
					try {
						// Save Image
						image.Save (imageURIText);
						imageURI.Text = imageURIText;
					} catch (Exception ex) {
						MessageBox.Show ("Error: " + ex.Message);
					}
				}
			} else {
				MessageBox.Show("Error: There is no image to save!.", "Error!");
			}
		}

		void resolutionValueChanged (object sender, EventArgs e)
		{
			// Initializing independent on resolution mode
			MessageBox.Show ("May take a few seconds", "Initializing");
			bool isComboBox = resolutionValue.Visible;
			if (isComboBox)
				Mosaic.initialize (getSquareSize ());
			else {
				int width=0; int height=0;
				getDimentions(out width, out height);
				Mosaic.initialize(width, height);
			}
		}

		void advanceButtonClicked (object sender, EventArgs e)
		{
			// Toggle between Advance And not advance
			resolutionValue.Visible = !resolutionValue.Visible;
			tilesPerRow.Visible = !tilesPerRow.Visible;
			tilesPerColumn.Visible = !tilesPerColumn.Visible;
			chooseFolderButton.Visible = !chooseFolderButton.Visible;
		}

		void chooseFolderButtonClicked (object sender, EventArgs e)
		{
			bool isComboBox = resolutionValue.Visible;

			int width=0; int height=0;
			if (isComboBox)
				width = height = getSquareSize ();
			else {
				getDimentions(out width, out height);
			}

			ChooseFolderWindow folderSettings = new ChooseFolderWindow(width, height);
			folderSettings.ShowDialog();
		}

		void createMosaicButtonClicked (object sender, EventArgs e)
		{
			if (mosaicImage.Image != null) {
				bool isComboBox = resolutionValue.Visible;
				int newHeight = 0;
				if (!int.TryParse (heightTextBox.Text.ToString (), out newHeight))
					MessageBox.Show ("Enter number please", "Error");
				int newWidth = 0;
				if (!int.TryParse (widthTextBox.Text.ToString (), out newWidth))
					MessageBox.Show ("Enter number please", "Error");

				// Create new mosaic
				Mosaic mosaic = new Mosaic (mosaicImage.Image, newWidth, newHeight);
				if (isComboBox)
					mosaic.tph = mosaic.tpw = getSquareSize ();
				else {
					int width=0; int height=0;
					getDimentions(out width, out height);
					mosaic.tpw = width;
					mosaic.tph = height;
					Mosaic.initialize(width, height);
				}


				// Get result image
				Bitmap resultImage = mosaic.getMosaic (creatingMosaicProgressBar);

				// Get new width and height
				widthTextBox.Text = mosaic.Width.ToString ();
				heightTextBox.Text = mosaic.Height.ToString ();

				// Set the image in picture box
				mosaicImage.Image = resultImage;

				// Set Dimention of new image in status bar
				mainImageDimention.Text = String.Format ("{0} {1}×{2}", 
							imageDimentionString, mosaicImage.Image.Width, mosaicImage.Image.Height);
			} else {
				MessageBox.Show("Error: Please insert an image!, Click on browse button", "Error!");
			}
		}
	}
}

