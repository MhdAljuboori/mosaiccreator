using System;

namespace System
{
	namespace Drawing
	{
		public static class Processing
		{
			public static void Process (this Bitmap img, Func<int,int,Color,Color> action)
			{
				for (int x = 0; x < img.Width; x++) {
					for (int y = 0; y < img.Height; y++) {
						img.SetPixel (x, y, action (x, y, img.GetPixel (x, y)));
					}
				}
			}
		}
	}
}